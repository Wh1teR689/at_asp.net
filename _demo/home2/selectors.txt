﻿На странице google.com выписать следующие CSS и XPATH селекторы:
1. Строка для ввода кейворда для поиска
2. Кнопка "Поиск в Google"
3. Кнопка "Мне повезет"

На странице результатов гугла:
1. Ссылки на результаты поиска (все)
2. 5-я буква о в Goooooooooogle (внизу, где пагинация)

На странице yandex.com:
1. Login input field
2. Password input field
3. "Enter" button in login form

На странице яндекс почты (у кого нет ящика - зарегистрируйте)
1. Ссылка "Входящие"
2. Ссылка "Исходящие"
3. Ссылка "Спам"
4. Ссылка "Удаленные"
5. Ссылка "Черновики"
6. Кнопка "Новое письмо"
7. Кнопка "Обновить"
8. Кнопка "Отправить" (на странице нового письма)
9. Кнопка "Пометить как спам"
10. Кнопка "Пометить прочитанным"
11. Кнопка "Переместить в другую директорию"
12. Кнопка "Закрепить письмо"
13. Селектор для поиска уникального письма

На странице яндекс диска
1. Кнопка загрузить файлы
2. Селектор для уникального файла на диске
3. Кнопка скачать файл
4. Кнопка удалить файл
5. Кнопка в корзину
6. Кнопка восстановить файл

